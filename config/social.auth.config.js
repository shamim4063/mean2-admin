module.exports = {

    'facebookAuth': {
        'clientID': 'your-client-id-here', // your App ID
        'clientSecret': 'your-client-secret-here', // your App Secret
        'callbackURL': 'http://oxyworldintl.com/api/auth/facebook/callback'
    },

    'googleAuth': {
        'clientID': 'your-client-id-here',
        'clientSecret': 'your-client-secret-here',
        'callbackURL': 'http://oxyworldintl.com/api/auth/google/callback'
    },

    'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://oxyworldintl.com/auth/twitter/callback'
    },
};