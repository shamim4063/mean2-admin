# Admin panel based on Angular 4, Bootstrap 4, NodeJS, ExpressJS, Mongoose, MongoDB,  and Angular CLI

MEAN Stack Architecture by modification of ng2-admin and included backend part with NodeJS, ExpressJS, MongoDB 

## Features
*  MongoDB,
*  Mongoose
*  ExpressJS
*  Redis Caching
*  NodeJS
*  PassportJS
*  JWT
*  Authorization
*  Authentication
*  Angular 4
*  Angular CLI
* TypeScript
* Webpack
* Responsive layout
* High resolution
* Bootstrap 4 CSS Framework
* Sass
* jQuery
* and many more!


## Requisite
* MongoDB >=3.4
* NodeJS 6.11.0
* NPM 3.10.10
* redis

##Installation
* git clone https://gitlab.com/shamim4063/mean2-admin.git
* cd mean2-admin
* npm i
* run mongodb service
* npm run seed [databasename] [username] [password]
* change the config/config.json with database name
* npm install gulp
* gulp serve:admin
* open another command prompt and go to the project dir
* npm start

