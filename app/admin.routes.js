
import authRoutes from './routes/admin/_authentication.router.js';
import config from '../config/config.json';
import roleRoutes from './routes/admin/_role.router.js';
import userRoutes from './routes/admin/_user.router.js';
import menuRoutes from './routes/admin/_menu.router.js';


import multer from 'multer';
import cors from 'cors';
import express from 'express';
import voucher_codes from 'voucher-code-generator';
var jwt = require('jsonwebtoken');

export default (app, router, passport) => {

    // ### Express Middlware to use for all requests
    router.use((req, res, next) => {

        // Make sure we go to the next routes and don't stop here...
        next();
    });


    function isLoggedIn(req, res, next) {
        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the home page
        res.send({ staus: 420 });
    }

    function checkPermission(req) {

        // var requestedUrl = req.url,
        //     requestedMethod = req.method,
        //     role = req.user.role,
        //     hasUrl = (role.resources[requestedUrl].isAllowed) == undefined ? false : role.resources[requestedUrl].isAllowed,
        //     hasAction = (role.resources[requestedUrl].permissions[requestedMethod]) == undefined ? false : role.resources[requestedUrl].permissions[requestedMethod];
        if (hasUrl && hasAction)
            return true;

        return false;
    }

    let isPermitted = (req, res, next) => {
        let role_name = req.user.role.name.toLowerCase();
        if (req.isAuthenticated && role_name == "admin") {
            next();
        } else {
            if (!req.isAuthenticated() || !checkPermission(req)) {
                res.send(401);
            }
            else
                next();
        }
    };

    // Define a middleware function to be used for all secured routes
    let auth = (req, res, next) => {
        let token = req.cookies.admin_token;
        jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
            if (err) {
                res.json({ success: false, message: 'Please login first' });
            } else {
                let user = decoded;
                req.user = user;
                next();
            }
        })
        // if (!req.isAuthenticated())
        //     res.json({ name: "Authentication Failed", message: "Please login first" });

        // else
        //     next();
    };


    // #### RESTful API Routes

    // ### Server Routes

    // Handle things like API calls,

    // #### Authentication routes

    // Pass in our Express app and Router.
    // Also pass in auth & admin middleware and Passport instance
    authRoutes(app, router, passport, auth, isPermitted);

    roleRoutes(app, router, auth, isPermitted);
    userRoutes(app, router, auth, isPermitted);
    menuRoutes(app, router, auth, isPermitted);

    // File Upload
    const path = require('path');
    app.use(cors());
    // const upload = multer({
    //     storage: multer.diskStorage({
    //         destination: 'uploads/',
    //         filename: (req, file, cb) => {
    //             let ext = path.extname(file.originalname);
    //             cb(null, `${Math.random().toString(36).substring(7)}${ext}`);
    //         }
    //     })
    // });    

    // app.post('/upload', upload.any(), (req, res) => {
    //     res.json(req.files.map(file => {
    //         let ext = path.extname(file.originalname);
    //         return {
    //             originalName: file.originalname,
    //             filename: file.filename
    //         }
    //     }));
    // });

    var fs = require('fs'),
        url = require('url'),
        dir = config.IMAGE_UPLOAD_PATH;

    app.get('/images/:model/:submodel/:size/:name', (req, res) => {
        var request = url.parse(req.url, true);
        var action = request.pathname;
        var model = req.params.model + '/';
        var submodel = req.params.submodel + '/';
        var size = req.params.size + '/';
        if (action == ('/images/' + model + submodel + size + req.params.name)) {
            var img = fs.readFileSync(dir + model + submodel + size + req.params.name);
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        } else {
            var img = fs.readFileSync(dir + 'no-photo.png');
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        }
    })

    app.get('/image/:model/:name', (req, res) => {
        var request = url.parse(req.url, true);
        var action = request.pathname;
        var model = req.params.model + '/';
        if (action == ('/image/' + model + req.params.name)) {
            var img = fs.readFileSync(dir + model + req.params.name);
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        } else {
            var img = fs.readFileSync(dir + 'no-photo.png');
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        }
    })


    // All of our routes will be prefixed with /admin/api
    app.use('/admin/api', router);
};
