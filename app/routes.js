
import multer from 'multer';
import cors from 'cors';
import express from 'express';
var jwt = require('jsonwebtoken');

export default (app, router, passport) => {

    // Redis Cache Server
    var cache = require('express-redis-cache')({
        host: process.env.REDIS_SERVER
    });
    // ### Express Middlware to use for all requests
    router.use((req, res, next) => {

        // Make sure we go to the next routes and don't stop here...
        next();
    });

    function isLoggedIn(req, res, next) {
        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the home page
        res.send({ staus: 420 });
    }

    // Define a middleware function to be used for all secured routes
    let auth = (req, res, next) => {
        if (!req.isAuthenticated())
            res.json({ name: "Authentication Failed", message: "Please login first" });

        else
            next();
    };

    // For mobile app api
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });



    const path = require('path');
    app.use(cors());

    const upload = multer({
        storage: multer.diskStorage({
            destination: 'uploads/member/',
            filename: (req, file, cb) => {
                let ext = path.extname(file.originalname);
                //cb(null, `${Math.random().toString(36).substring(7)}${ext}`);
                cb(null, `${req.query.id}${ext}`)
            }
        })
    });

    app.post('/upload', upload.any(), (req, res) => {

        res.json(req.files.map(file => {
            let ext = path.extname(file.originalname);
            return {
                originalName: file.originalname,
                filename: file.filename
            }
        }));
    });


    app.use('/api', router);

    // Route to handle all Angular requests
    app.get('*', (req, res) => {
        // Load our src/app.html file
        //** Note that the root is set to the parent of this folder, ie the app root **

        if (process.env.MODULE && process.env.MODULE == 'public') {
            res.sendFile('/dist-public/index.html', { root: __dirname + "/../" });
        } else if (process.env.MODULE && process.env.MODULE == 'mobile') {
            res.sendFile('/dist-mobile/index.html', { root: __dirname + "/../" });
        } else {
            res.sendFile('/dist-admin/index.html', { root: __dirname + "/../" });
        }
    });

};
