// */app/routes/_authentication.router.js*

// Load user model
import config from '../../../config/config.json'
import User from '../../models/user.model.js';
import Role from '../../models/role.model';

// Load the Mongoose ObjectId function to cast string as ObjectId
let ObjectId = require('mongoose').Types.ObjectId;
var jwt = require('jsonwebtoken');

export default (app, router, passport, auth, isPermitted) => {

  // Route to test if the user is logged in or not
  router.get('/auth/loggedIn', (req, res) => {
    // If the user is authenticated, return a user object
    // else return 0
    // res.json(req.user)
    res.send(req.isAuthenticated() ? req.user : '0');
  });

  router.post('/auth/login', (req, res, next) => {

    passport.authenticate('local-login', (err, user, info) => {

      if (err)
        return next(err);

      if (!user) {

        // Set HTTP status code `401 Unauthorized`
        // res.status(401);

        return res.json({ message: info.loginMessage });
      }

      req.login(user, (err) => {

        if (err)
          return next(err);

        // Set HTTP status code `200 OK`
        res.status(200);

        //  return  jwt token
        let super_secret = config.SESSION_SECRET;
        var usr = { _id: user._id, agent: user.agent, distributor: user.distributor, area: user.area, store: user.store, local: user.local, user_type: user.user_type, role: { _id: user.role._id, name: user.role.name } }
        var token = jwt.sign(usr, super_secret, { expiresIn: 60 * 60 });

        res.cookie('admin_token', token);
        res.json({
          user: user,
          success: true,
          token: token
        });

        // Return the user object
        // res.send(req.user);
      });

    })(req, res, next);
  });

  router.post('/auth/signup', (req, res, next) => {
    // var regStatus={};
    // Call `authenticat=-000e()` from within the route handler, rather than
    // as a route middleware. This gives the callback access to the `req`
    // and `res` object through closure.

    // If authentication fails, `user` will be set to `false`. If an
    // exception occured, `err` will be set. `info` contains a message
    // set within the Local Passport strategy.
    passport.authenticate('local-signup', (err, user, info) => {

      if (err)
        return next(err);

      // If no user is returned...
      if (!user) { return res.json({ message: info.signupMessage }) }

      // Set HTTP status code `204 No Content`
      // res.sendStatus(204);
      res.json({ _id: user._id, message: "User Add Success....." })

    })
      // res.send(regStatus);
      (req, res, next);
  });

  router.post('/auth/logout', (req, res) => {
    req.logOut();
    res.clearCookie("token");
    res.json({ success: true });
  });

  // Route to get the current user
  // The `auth` middleware was passed in to this function from `routes.js`
  router.get('/auth/user', auth, (req, res) => {

    // Send response in JSON to allow disassembly of object by functions
    res.json(req.user);
  });

  // Route to delete a user. Accepts a url parameter in the form of a
  // username, user email, or mongoose object id.
  // The `admin` Express middleware was passed in from `routes.js`
  router.delete('/auth/delete/:uid', isPermitted, (req, res) => {

    User.remove({

      // Model.find `$or` Mongoose condition
      $or: [

        { 'local.username': req.params.uid },

        { 'local.email': req.params.uid },

        { '_id': ObjectId(req.params.uid) }
      ]
    }, (err) => {

      // If there are any errors, return them
      if (err)
        return next(err);

      // HTTP Status code `204 No Content`
      res.sendStatus(204);
    });
  });

  /*router.put('/auth/change-password', (req, res, next) => {



    passport.authenticate('local-auth-change-password', (err, user, info) => {
      if (err)
        return next(err);
      if (!user) {
        return res.json({ message: info.loginMessage });
      }
      console.log(user);
      req.login(user, (err) => {
        if (err)
          return next(err);

        res.status(200);
        res.json({ message: "Success Update Password" });
      });

    })(req, res, next);


  });*/

  router.route('/auth/change-password')
    .put((req, res, next) => {

      let token = req.cookies.admin_token || req.headers['admin_token'];
      // res.json({ userename: "mamun", message: 'Success Update Password' });
      // console.log(token);
      jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
        if (err) {
          console.log(err);
          res.json({ success: false, message: 'Please login first' });
        } else {

          let user = decoded;
          req.body.username = user.local.username;
          // req.body.password = "Mamun@199";
          //  console.log(req.body);
          passport.authenticate('local-auth-change-password', (err, user, info) => {
            if (err)
              return next(err);
            if (!user) {
              return res.json({ message: info.loginMessage });
            }
            req.login(user, (err) => {
              if (err)
                return next(err);
              res.status(200);
              res.json({ userename: user.local.username, message: 'Success Update Password' });
            });

          })(req, res, next);

        }
      })
    });
};
