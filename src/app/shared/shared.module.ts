import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiLanguageInputComponent } from './components/multi-language-input/multi-language-input.component';
import { BbImageUploader, BbImageUploaderService } from './components/bb-image-uploader';
import { TypeaheadComponent } from './components/typeahead';
import { InputTypeaheadComponent } from './components/input-typeahead';
// import {} from 

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        MultiLanguageInputComponent,
        BbImageUploader,
        TypeaheadComponent,
        InputTypeaheadComponent
    ],
    exports: [
        MultiLanguageInputComponent,
        BbImageUploader,
        TypeaheadComponent,
        InputTypeaheadComponent
    ],
    providers: [BbImageUploaderService]
})
export class SharedModule { }