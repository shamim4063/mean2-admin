export class CountryStore {
    public countries = [
        { name: 'America', code: '+1' },
        { name: 'Australia', code: '+61 ' },
        { name: 'Bangladesh', code: '+88' },
        { name: 'Brazil', code: '+55' },
        { name: 'India', code: '+91' },
        { name: 'Pakistan', code: '+92' },
        { name: 'China', code: '+86' },
        { name: 'Germany', code: '+49' },
        { name: 'Hong Kong', code: '+852' },
        { name: 'Japan', code: '+81' },
        { name: 'Malaysia', code: '+60' },
        { name: 'Nepal', code: '+977' },
        { name: 'New Zealand', code: '+64 ' },
        { name: 'Sri Lanka', code: '+94' }
    ]
}