import { Injector, Injectable } from "@angular/core";
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pluck';

import { BaseService } from './base-service';
import { PaginatedResult } from '../interfaces/paginated-result.interface';

@Injectable()
export class PaginationStoreService extends BaseService {
    protected entityName = 'Item';

    private _results: BehaviorSubject<PaginatedResult<any>>;
    protected _items: Observable<any[]>;
    protected _count: Observable<number>;
    protected _resultStore: {
        results: {
            items: Observable<any[]>,
            count: number
        }
    };

    private notificationOptions = { timeOut: 2000, showProgressBar: false, pauseOnHover: false, clickToClose: true, maxLength: 50 }

    constructor(injector: Injector) {
        super(injector);
    }

    init() {
        super.init();
        this._resultStore = { results: { items: null, count: 0 } };
        this._results = <BehaviorSubject<PaginatedResult<any>>>new BehaviorSubject({});
        // this._items = this._results.pluck<any[]>('items');
        // this._count = this._results.pluck<number>('count');
    }

    protected get items() {
        return this._items;
    }

    get count() {
        return this._count;
    }

    protected add(item: any) {
        return this.http.post(this.apiUrl, JSON.stringify(item), { headers: this.headers })
            .map(res => res.json()).subscribe(item => {
                // this._resultStore.results.push(item);
                this._results.next(Object.assign({}, this._resultStore).results);
                // this.notificationsService.success('Add ' + this.entityName, this.entityName + ' added successfully', this.notificationOptions);
            }, error => {
                console.log('error', error);
                // this.notificationsService.error(error.name, error.message);
            });
    }

    protected update(item: any) {
        return this.http.put(this.apiUrl + item._id, JSON.stringify(item), { headers: this.headers })
            .map(res => res.json()).subscribe(item => {
                // this._resultStore.results.items.forEach((t, i) => {
                //     if (t._id === item._id) {
                //         this._resultStore.results[i] = item;
                //     }
                // });
                this._results.next(Object.assign({}, this._resultStore).results);
                // this.notificationsService.success('Edit ' + this.entityName, this.entityName + ' saved successfully');
            }, error => {
                console.log('error', error);
                // this.notificationsService.error(error.name, error.message);
            });
    }

    protected delete(id: string) {
        return this.http.delete(this.apiUrl + id)
            .map(res => res.json()).subscribe(item => {
                // this._resultStore.results.forEach((t, i) => {
                //     if (t._id === id) {
                //         this._resultStore.results.splice(i, 1);
                //     }
                // });
                this._results.next(Object.assign({}, this._resultStore).results);
                // this.notificationsService.success('Delete ' + this.entityName, this.entityName + ' deleted successfully');
            }, error => {
                console.log('error', error);
                // this.notificationsService.error(error.name, error.message);
            });
    }

    protected getAll() {
        return this.http.get(this.apiUrl).map((res: Response) => res.json()).subscribe(results => {
            this._resultStore.results = results;
            this._results.next(Object.assign({}, this._resultStore).results);
        }, error => {
            console.log('error', error)
        });
    }

    protected getById(id: string) {
        return this.http.get(this.apiUrl + id).map((res: Response) => {
            return res.json();
        })
    }

    protected getPaged(page: number = 1, itemsPerPage: number = 10) {
        let paginationHeaders = new Headers();
        paginationHeaders.append('Content-Type', 'application/json');
        paginationHeaders.append('bz-pagination', page + ',' + itemsPerPage);

        return this.http.get(this.apiUrl, { headers: paginationHeaders })
            .map((res: Response) => {
                return res.json()
            }).subscribe(results => {
                this._resultStore.results = results;
                this._results.next(Object.assign({}, this._resultStore).results);
            });
    }

    protected search(searchTerms: any = null, page: number = 1, itemsPerPage: number = 10) {
        var params = new URLSearchParams();
        if (searchTerms) {
            params.set('search', searchTerms);
        }
        params.set('page', String(page));
        params.set('itemsPerPage', String(itemsPerPage));

        return this.http.get(this.apiUrl + 'search/v1', { search: params })
            .map((res: Response) => res.json())
            .subscribe(results => {
                this._resultStore.results = results;
                this._results.next(Object.assign({}, this._resultStore).results);
            });
    }
}