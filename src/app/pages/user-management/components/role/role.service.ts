import { Injector, Injectable } from "@angular/core";
import { TreeviewItem } from '../../../../tree';
import { BaseService } from '../../../../shared/services/base-service';

@Injectable()
export class RoleService extends BaseService {

  constructor(injector: Injector) {
    super(injector);
  }

  init() {
    super.init();
    this.apiPath = 'role';
  }

  getAll() {
    return this.http.get(this.apiUrl).map(res => res.json());
  }

  get(id) {
    return this.http.get(this.apiUrl + id + '/').map(res => res.json());
  }

  add(role) {
    return this.http.post(this.apiUrl, JSON.stringify(role), { headers: this.headers })
      .map(res => res.json());
  }

  update(role) {
    return this.http.put(this.apiUrl + role._id, JSON.stringify(role), { headers: this.headers })
      .map(res => res.json());
  }

  delete(roleId) {
    return this.http.delete(this.apiUrl + roleId)
      .map(res => res.json());
  }






  // getProducts(): TreeviewItem[] {
  //   const fruitCategory = new TreeviewItem({
  //     text: 'Fruit', value: 1, name: 'test', children: [
  //       { text: 'Apple', value: 11, name: 'test'},
  //       { text: 'Mango', value: 12, name: 'test', children: [{ text: 'Test', value: 12, name: 'test' }] }
  //     ]
  //   });
  //   const vegetableCategory = new TreeviewItem({
  //     text: 'Vegetable', value: 2, name: 'test', children: [
  //       { text: 'Salad', value: 21, name: 'test' },
  //       { text: 'Potato', value: 22, name: 'test' }
  //     ]
  //   });
  //   vegetableCategory.children.push(new TreeviewItem({ text: 'Mushroom', value: 23, name: 'test', checked: false }));
  //   vegetableCategory.correctChecked(); // need this to make 'Vegetable' node to change checked value from true to false
  //   return [fruitCategory, vegetableCategory];
  // }

}